
"""
test tcs34725 using tcs345725 class library
"""

import sys
from time import sleep
from machine import Pin, SoftI2C

from tcs34725 import *                                  # class library

def main():
    print("Starting tcs34725_test program")
    if sys.platform == "pyboard":                       # test with PyBoard
        tcs = TCS34725(scl=Pin("B6"), sda=Pin("B7"))    # instance of TCS34725 on pyboard
    else:                                               # test with ESP32 board
        tcs = TCS34725(scl=Pin(4), sda=Pin(15))         # instance of TCS34725 on ESP32
    if not tcs.isconnected:                             # terminate if not connected
        print("Terminating...")
        sys.exit()
    tcs.gain = TCSGAIN_LOW
    tcs.integ = TCSINTEG_HIGH
    tcs.autogain = True                                 # use autogain!

    color_names = ("Clear", "Red", "Green", "Blue")

    print(" Clear   Red Green  Blue    gain  >")
    try:
        while True:                                     # forever
            """ show color counts """
            counts_tuple = tcs.colors                   # obtain all counts
            counts = list(counts_tuple)                 # list of 4 counts
            for count in counts_tuple:
                # show 'absolute' light value (count / gain-factor)
                if count >= tcs.overflow_count:         # overflow?
                    count = -1                          # overflow reported as count -1
                print(" {:5d}".format(count // tcs.gain_factor), end="")
            # not sure if this method to find the dominant color always works
            largest = max(counts[1:])                   # largest count of RGB
            avg = sum(counts[1:]) // 3                  # average of color counts
            if largest > avg * 3 // 2:                  # largest 50% over average
                color = color_names[counts.index(largest)]
            else:
                color = "-"
            print("    ({:2d})  {:s}" .format(tcs.gain_factor, color))
            sleep(5)                                    # interval between reads

    except KeyboardInterrupt:
        print("Closing down!")

    except Exception as err:
        print("Exception:", err)

    tcs.close()

main()


#
