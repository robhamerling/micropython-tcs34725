# micropython-tcs34725

Simple driver class for TCS34725 and TCS34727 color sensors

Supports only reading of all colors by polling, optionally with automatic gain control.

Does not support thresholds, interrupts, power management.
